package e.flabe.bluetoothmulti;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mdg.androble.ClientSocket;
//import com.mdg.androble.BluetoothActivity;
import com.mdg.androble.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends BluetoothActivity {
    BluetoothManager bluetoothManager;
    receiveMessage rm;
    ServerClass serverClass;
    Button listen, send, listDevices, enableConect;
    ListView listView;
    TextView msg_box, status;
    EditText writeMsg;

    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Teste";
    File root = android.os.Environment.getExternalStorageDirectory();
    File dir = new File(path);
    File file = new File(path, "/myData.txt");

    public ArrayList<UUID> mUuids;
    int UUID_TO_USE = 0;
    String type = "";

    BluetoothAdapter myBluetoothAdapter;
    BluetoothDevice[] btArray;

    SendReceive sendReceive;

    private ArrayList<BluetoothSocket> mSockets;
    private ArrayList<SendReceive> sendReceiveThread;

    static final int STATE_LISTENING = 1;
    static final int STATE_CONNECTING = 2;
    static final int STATE_CONNECTED = 3;
    static final int STATE_CONNECTION_FAILED = 4;
    static final int STATE_MESSAGE_RECEIVED = 5;
    int REQUEST_ENABLE_BLUETOOTH = 1;

    private static final String APP_NAME = "BluetoothComm";
    private static final UUID MY_UUID = UUID.fromString("658fcda0-3433-11e8-b467-0ed5f89f718b");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bluetoothManager= BluetoothManager.getInstance();
        findViewByIdes();

        dir.mkdirs();

        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        rm = new receiveMessage();
        bluetoothManager.setMessageObject(rm);
        mSockets = new ArrayList<BluetoothSocket>();
        sendReceiveThread = new ArrayList<SendReceive>();

        if(!myBluetoothAdapter.isEnabled()){
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH);
        }
        implementListeners();

        mUuids = new ArrayList<UUID>();
        mUuids.add(UUID.fromString("658fcda0-3433-11e8-b467-0ed5f89f718b"));
        mUuids.add(UUID.fromString("b7746a40-c758-4868-aa19-7ac6b3475dfc"));
        mUuids.add(UUID.fromString("2d64189d-5a2c-4511-a074-77f199fd0834"));
        mUuids.add(UUID.fromString("e442e09a-51f3-4a7b-91cb-f638491d1412"));
        mUuids.add(UUID.fromString("a81d6504-4536-49ee-a475-7d96d09439e4"));
    }

    private void implementListeners() {
        listDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<BluetoothDevice> bt = myBluetoothAdapter.getBondedDevices();
                btArray = new BluetoothDevice[bt.size()];
                String[] strings = new String[bt.size()];
                int index = 0;

                if(bt.size()>0){
                    for(BluetoothDevice device : bt){
                        btArray[index]=device;
                        strings[index]=device.getName();
                        index++;
                    }
                    ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, strings);
                    listView.setAdapter(arrayAdapter);
                }
            }
        });

        listen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                serverClass=new ServerClass();
                serverClass.start();
                status.setText("Listening Connections");
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    for(int i = 0; i < 4; i++) {
                        status.setText("Connecting");
                        ClientClass clientClass = new ClientClass(btArray[position], mUuids.get(i));
                        clientClass.start();
                    }
                } catch (Exception e) {
                    status.setText("Failed");
                }

            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread() {
                    int j = 0;
                    public void run() {
                        while (j++ < 1000) {
                            try {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String string= String.valueOf(writeMsg.getText());
                                        Long tsLong = System.currentTimeMillis();
                                        String ts = tsLong.toString();
                                        string+= "\n" + ts;
                                        string = ts;
                                        if (type == "SERVER") {
                                            for (int i = 0; i < sendReceiveThread.size(); i++) {
                                                sendReceiveThread.get(i).write(string.getBytes());
                                            }
                                        } else {
                                            sendReceive.write(string.getBytes());
                                        }
                                        string = "";
                                    }
                                });
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.start();
            }
        });

        /*send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int j = 0 ; j < 200; j++) {
                    String string= String.valueOf(writeMsg.getText());
                    Long tsLong = System.currentTimeMillis();
                    String ts = tsLong.toString();
                    string+= "\n" + ts;
                    if (type == "SERVER") {
                        for (int i = 0; i < sendReceiveThread.size(); i++) {
                            sendReceiveThread.get(i).write(string.getBytes());
                        }
                    } else {
                        sendReceive.write(string.getBytes());
                    }
                    //break;
                    string = "";
                }
            }
        }); */

        enableConect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //type = "SERVER";
                //bluetoothManager.Type(type);
            }
        });
    }

    private void findViewByIdes() {
        listen = (Button) findViewById(R.id.listen);
        enableConect = (Button) findViewById(R.id.enableConect);
        send = (Button) findViewById(R.id.send);
        listDevices = (Button) findViewById(R.id.listDevices);
        listView = (ListView) findViewById(R.id.listView);
        msg_box = (TextView) findViewById(R.id.msg);
        status = (TextView) findViewById(R.id.status);
        writeMsg = (EditText) findViewById(R.id.writemsg);

        msg_box.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
    }

    Handler handler= new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg){
            switch (msg.what){
                case STATE_LISTENING:
                    status.setText("listening");
                    break;
                case STATE_CONNECTING:
                    status.setText("connecting");
                    break;
                case STATE_CONNECTED:
                    status.setText("connected");
                    break;
                case STATE_CONNECTION_FAILED:
                    status.setText("failed");
                    break;
                case STATE_MESSAGE_RECEIVED:
                    byte[] readBuffer= (byte[])msg.obj;
                    String tempMsg=new String(readBuffer, 0,msg.arg1);
                    Long tsLong = System.currentTimeMillis();
                    String ts = tsLong.toString();
                    msg_box.setText(tempMsg + "\n" + ts);
                    status.setText("message received");
                    writeToFile(tempMsg, ts);
                    break;
            }
            return true;
        }
    });

    public void writeToFile(String s1, String s2){
        try {
            String dif;
            Long num1, num2, num_dif;
            num1 = Long.parseLong(s1);
            num2 = Long.parseLong(s2);
            if(num2 > num1){
                num_dif = num2 - num1;
                Log.wtf("Dif", Long.toString(num_dif));
            }else{
                num_dif = num1 - num2;
                Log.wtf("Dif", Long.toString(num_dif));
            }
            dif = "Dif: ";
            dif += Long.toString(num_dif);
            FileOutputStream writer = null;
            writer = new FileOutputStream(file, true);
            writer.write(s1.getBytes());
            writer.write("\n".getBytes());
            writer.write(s2.getBytes());
            writer.write("\n".getBytes());
            writer.write(dif.getBytes());
            writer.write("\n".getBytes());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ServerClass extends Thread{
        private BluetoothServerSocket serverSocket;

        public ServerClass(){
            type = "SERVER";
        }

        public void run(){
            type = "SERVER";
            BluetoothSocket socket=null;

            try {
                //msg_box.setText("oi");
                for(int i = 0; i < 4; i++) {
                    serverSocket = myBluetoothAdapter.listenUsingRfcommWithServiceRecord(APP_NAME, mUuids.get(i));
                    socket = serverSocket.accept();
                    if (socket != null) {
                        String address = socket.getRemoteDevice().getAddress();
                     //   mSockets.add(socket);
                     //   mDeviceAddresses.add(address);
                        Message message = Message.obtain();
                        message.what=STATE_CONNECTED;
                        handler.sendMessage(message);

                        sendReceive = new SendReceive(socket);
                        sendReceive.start();
                        sendReceiveThread.add(sendReceive);
                    }
                }
            } catch (IOException e) {
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
                e.printStackTrace();
            }
        }
    }

    private class ClientClass extends Thread{
        private BluetoothDevice device;
        private BluetoothSocket socket;

        public ClientClass(BluetoothDevice device1, UUID uuidToTry){
            device = device1;
            type = "CLIENT";
            try {
                socket=device.createRfcommSocketToServiceRecord(uuidToTry);
                //UUID_TO_USE = UUID_TO_USE + 1;
            } catch (IOException e) {
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
                e.printStackTrace();
            }

        }
        public void run(){
            try {
                socket.connect();
                Message message = Message.obtain();
                message.what = STATE_CONNECTED;
                handler.sendMessage(message);

                sendReceive = new SendReceive(socket);
                sendReceive.start();
                sendReceiveThread.add(sendReceive);
            } catch (IOException e) {
                e.printStackTrace();
                //Message message = Message.obtain();
                //message.what = STATE_CONNECTION_FAILED;
                //handler.sendMessage(message);
            }
        }
    }

    private class SendReceive extends Thread{
        private final BluetoothSocket bluetoothSocket;
        private final InputStream inputStream;
        private final OutputStream outputStream;
        //private final BluetoothSocket
        //Set<BluetoothSocket> btSockets = new LinkedHashSet<BluetoothSocket>();
        ArrayList<BluetoothSocket> btSockets = new ArrayList<BluetoothSocket>();
        //ArrayList<InputStream> Inputs = new ArrayList<InputStream>();
        //ArrayList<OutputStream> Outputs = new ArrayList<OutputStream>();

        public SendReceive(BluetoothSocket socket){
            bluetoothSocket = socket;
            btSockets.add(bluetoothSocket);

            InputStream tempIn = null;
            OutputStream tempOut = null;

            try {
                tempIn = bluetoothSocket.getInputStream();
                tempOut = bluetoothSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            inputStream=tempIn;
            outputStream=tempOut;
            //Inputs.add(tempIn);
            //Outputs.add(tempOut);
        }

        public void run(){
            byte[] buffer=new byte[1024];
            int bytes;

            while(true){
                try {
                    bytes = inputStream.read(buffer);
                    handler.obtainMessage(STATE_MESSAGE_RECEIVED, bytes, -1, buffer).sendToTarget();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void write(byte[] bytes){
            try {
                System.out.println("bytes" + bytes);
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class receiveMessage implements Observer {
        @Override
        public void update(Observable observable, Object data) {
            //String msg = ((msg_box)observable).getMessage();
            String msg = data.toString();
            //handler.sendMessage(msg);
            //Toast.makeText(MainActivity.this,msg,Toast.LENGTH_LONG).show();
            //msg_box.setText(msg);
            //do whatever you want to do with received message
        }
    }

}

